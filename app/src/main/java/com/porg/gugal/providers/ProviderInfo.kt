/*
 *     ProviderInfo.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers

data class ProviderInfo(val name: String, val description: String, var titleInSearchBox: String, val pros: Array<String>, val cons: Array<String>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProviderInfo

        if (name != other.name) return false
        if (description != other.description) return false
        if (titleInSearchBox != other.titleInSearchBox) return false
        if (!pros.contentEquals(other.pros)) return false
        if (!cons.contentEquals(other.cons)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + titleInSearchBox.hashCode()
        result = 31 * result + pros.contentHashCode()
        result = 31 * result + cons.contentHashCode()
        return result
    }
}