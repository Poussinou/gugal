/*
 *     DummySerp.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.porg.gugal.Result

class DummySerp: SerpProvider {
    override val SerpProvider.Companion.id: String
        get() = "225fa1a8022149648dd989f7a803360c-dummy"

    @Composable
    override fun ConfigComposable(modifier: Modifier) {
        Text(
            text = "This is a dummy provider. Please change the provider to search the web.",
            modifier = Modifier.padding(all = 4.dp).then(modifier)
        )
    }

    override fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("token" to "123456789abcdef")
    }

    override fun search(query: String): Array<Result?> {
        return Array(20,){Result("Dummy result $it",
            "A search was performed using the dummy provider. Please change the provider to search the web.",
        "https://example.com", "dummyresult.com")}
    }

    //override val providerInfo = ProviderInfo("Dummy", "A dummy provider.", "Dummy", Array(3){"Pro $it"}, Array(3){"Con $it"})
    override val providerInfo: ProviderInfo? = null
}