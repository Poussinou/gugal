/*
 *     GoogleCseSerp.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers.cse

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.SerpProvider
import java.util.*

class GoogleCseSerp: SerpProvider {
    @Composable
    override fun ConfigComposable(modifier: Modifier) {
        val _cx = remember { mutableStateOf(TextFieldValue()) }
        val _ak = remember { mutableStateOf(TextFieldValue()) }
        Column(
            modifier = Modifier.padding(all = 4.dp).then(modifier)
        ) {
            Text(
                text = "Go to cse.google.com. Click Add, name your engine," +
                        " select \"Search the entire web\" and enable image search. Click Customize," +
                        " copy the search engine ID and paste it here:",
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            TextField(
                placeholder = { Text(text = "CSE ID") },
                value = _cx.value,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth(),
                onValueChange = { nv ->
                    _cx.value = nv
                    cx = _cx.value.text
                },
                keyboardActions = KeyboardActions(
                    onDone = {
                        cx = _cx.value.text
                    }
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                )
            )
            Text(
                text = "Now scroll down to \"Programmatic access\", then tap \"Get started\"" +
                        " next to \"Custom Search JSON API\". Click \"Get a key\", go through the setup and paste the API key here:",
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            TextField(
                placeholder = { Text(text = "API key") },
                value = _ak.value,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth(),
                onValueChange = { nv ->
                    _ak.value = nv
                    apikey = nv.text
                },
                keyboardActions = KeyboardActions(
                    onDone = {
                        apikey = _ak.value.text
                    }
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                )
            )
            Text(
                text = "I recommend tapping the \"API Console\" link, and restricting the API key to Custom Search API.",
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }

    override fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("ak" to apikey, "cx" to cx)
    }

    private var apikey = ""
    private var cx = ""

    override val SerpProvider.Companion.id: String get() = "abf621d5a5df4e4ba2ea70dd9362f1a3-goog"

    override val providerInfo: ProviderInfo?
        get() = TODO("Not yet implemented")
}