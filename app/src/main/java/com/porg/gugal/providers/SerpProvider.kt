/*
 *     SerpProvider.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.porg.gugal.Result
import java.util.*

interface SerpProvider {
    @Composable fun ConfigComposable(modifier: Modifier) {}

    fun search(query: String): Array<Result?> {
        return Array(0){null}
    }

    fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("key" to "value")
    }

    companion object

    val Companion.id: String get() = ""
    val id: String get() = Companion.id

    val providerInfo: ProviderInfo?
}