/*
*     ResultsPage.kt
*     Gugal
*     Copyright (c) 2022 thegreatporg
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.porg.gugal

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.setup.SetupStartActivity
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.gugal.ui.theme.shapeScheme
import org.json.JSONObject

// TODO SPFW: make SERP provider selectable
val serpProvider: SerpProvider = GoogleCseSerp()
val cardElevation = 15.dp

@ExperimentalAnimationApi
@Composable
fun ResultPage(context: Context, _apikey: String, _cx: String) {
    if (_cx.isEmpty() && _apikey.isEmpty()) {
        val intent = Intent(context, SetupStartActivity::class.java)
        intent.component =
            ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupStartActivity")
        intent.flags = FLAG_ACTIVITY_NEW_TASK
        startActivity(context, intent, null)
    }
    val textState = remember { mutableStateOf(TextFieldValue()) }
    Column {
        val res = remember { mutableStateListOf<Result>() }

        TextField(
            placeholder = { Text(text = "Search Google")},
            value = textState.value,
            modifier = Modifier
                .padding(all = 4.dp)
                .fillMaxWidth(),
            onValueChange = { nv ->
                textState.value = nv
            },
            keyboardActions = KeyboardActions(
                onSearch = {
                    val queue: RequestQueue = Volley.newRequestQueue(context)
                    val tsvt = textState.value.text
                    val url = "https://customsearch.googleapis.com/customsearch/v1?cx=${_cx}&key=${_apikey}&q=$tsvt"
                    // Request a string response from the provided URL.
                    val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                        { response ->
                            val items = response.getJSONArray("items")
                            res.clear()
                            for (i in 0 until items.length()) {
                                val item: JSONObject = items.getJSONObject(i)
                                if (item.has("snippet"))
                                    res.add(Result(item.getString("title"), item.getString("snippet"),
                                        item.getString("link"), item.getString("displayLink")))
                                else
                                    res.add(Result(item.getString("title"), null,
                                        item.getString("link"), item.getString("displayLink")))
                            }
                        },
                        { error ->
                            error.message?.let {
                                res.add(Result("Error", it, "",""))
                            }
                        }
                    )

                    // Access the RequestQueue through your singleton class.
                    queue.add(jsonObjectRequest)
                }
            ),
            maxLines = 1,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            )
        )

        Results(
            results = res,
            context
        )
    }
}

@Composable
fun ResultCard(res: Result, context: Context?) {
    Surface(
        shape = MaterialTheme.shapeScheme.medium,
        tonalElevation = cardElevation,
        modifier = Modifier
            .padding(all = 4.dp).fillMaxWidth()
            .clickable(onClick = {
                if (res.url != "") {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(res.url))
                    // Note the Chooser below. If no applications match,
                    // Android displays a system message.So here there is no need for try-catch.
                    startActivity(context!!, Intent.createChooser(intent, "Open result in"), null)
                }
            }),
    ) {
        Column {
            Text(
                text = res.title,
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.titleLarge
            )
            if (res.domain != "") {
                Text(
                    text = res.domain,
                    modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, start = 4.dp, end = 4.dp),
                    style = MaterialTheme.typography.titleSmall
                )
            }
            if (res.body != null) {
                // Add a vertical space between the author and message texts
                Spacer(modifier = Modifier.height(2.dp))
                Text(
                    text = res.body,
                    modifier = Modifier.padding(all = 4.dp),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
        }
    }
}

@Composable
@Preview
fun PreviewResultCard() {
    GugalTheme {
        ResultCard(
            res = Result("Colleague", "Hey, take a look at Jetpack Compose, it's great!", "about:blank", "test.com"),
            context = null
        )
    }
}

@ExperimentalAnimationApi
@Composable
fun Results(results: List<Result>, applicationContext: Context?) {
    LazyColumn {
        items(results) { message ->
            ResultCard(message, applicationContext)
        }
    }
}

@ExperimentalAnimationApi
@Composable
@Preview
fun ResultsPreview() {
    GugalTheme {
        Results(
            results = List(size = 3) {
                Result(
                    "Google privacy scandal",
                    "Google have been fined for antitrust once again, a few days after people bought more than 5 Pixels.",
                    "about:blank",
                    "test.com"
                )
            },
            applicationContext = null
        )
    }
}