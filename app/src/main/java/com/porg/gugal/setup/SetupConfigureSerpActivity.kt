/*
 *     SetupConfigureSerpActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.porg.gugal.Material3SetupWizard
import com.porg.gugal.R
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.ui.theme.GugalTheme

class SetupConfigureSerpActivity : ComponentActivity() {

    // TODO SPFW: make SERP provider selectable
    val serpProvider: SerpProvider = GoogleCseSerp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Material3SetupWizard.TwoButtons(
                        positive = { saveSensitive(serpProvider) },
                        positiveText = "Save",
                        negative = { finish() }
                    )
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Material3SetupWizard.Header(
                            text = getText(R.string.setup_p3_title).toString(),
                            doFinish = { finish() }
                        )
                        serpProvider.ConfigComposable(
                            modifier = Material3SetupWizard.PaddingModifier
                        )
                    }
                }
            }
        }
    }

    private fun saveSensitive(serpProvider: SerpProvider) {
        // Get the sensitive data
        val sensitive = serpProvider.getSensitiveCredentials()
        Log.d("gugal", sensitive.size.toString())

        // Although you can define your own key generation parameter specification, it's
        // recommended that you use the value specified here.
        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val sharedPrefsFile = "gugalprefs"
        val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            sharedPrefsFile,
            mainKeyAlias,
            applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        with (sharedPreferences.edit()) {
            // Edit the user's shared preferences...
            for (i in sensitive) {
                this.putString("serp_${serpProvider.id}_${i.key}", i.value)
            }
            apply()
        }

        val intent = Intent(applicationContext, SetupFOSSActivity::class.java)
        intent.component =
            ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupFOSSActivity")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

}