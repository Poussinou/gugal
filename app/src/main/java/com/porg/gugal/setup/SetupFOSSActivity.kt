/*
 *     SetupFOSSActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.porg.gugal.MainActivity
import com.porg.gugal.Material3SetupWizard
import com.porg.gugal.Material3SetupWizard.Companion.Tip
import com.porg.gugal.R
import com.porg.gugal.ui.theme.GugalTheme

class SetupFOSSActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterialApi::class,
        androidx.compose.animation.ExperimentalAnimationApi::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Material3SetupWizard.TwoButtons(
                        positive = {
                            val intent = Intent(applicationContext, MainActivity::class.java)
                            intent.component =
                                ComponentName("com.porg.gugal", "com.porg.gugal.MainActivity")
                            intent.flags = FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        },
                        positiveText = "Next",
                        negative = {
                            finish()
                        }
                    )
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Material3SetupWizard.Header(
                            text = getText(R.string.setup_p4_title).toString(),
                            doFinish = { finish() }
                        )
                        Text(
                            text = getText(R.string.setup_p4_description).toString(),
                            modifier = Modifier
                                .padding(start = 24.dp, top = 0.dp, bottom = 24.dp, end = 24.dp)
                                .fillMaxWidth(),
                            style = MaterialTheme.typography.bodyLarge
                        )
                        Tip(
                            text = getText(R.string.setup_p4_tip_1).toString(),
                            modifier = Modifier.padding(start = 24.dp, top = 0.dp, bottom = 24.dp, end = 24.dp),
                            image = R.drawable.ic_warning,
                            onClick = {
                                val intent = Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://gitlab.com/narektor/gugal/-/blob/setup-wizard/WHY_OFFICIAL.md"))
                                // Note the Chooser below. If no applications match,
                                // Android displays a system message.So here there is no need for try-catch.
                                startActivity(Intent.createChooser(intent, "Open result in"))
                            }
                        )
                        Tip(
                            text = getText(R.string.setup_p4_tip_2).toString(),
                            modifier = Modifier.padding(start = 24.dp, top = 0.dp, bottom = 24.dp, end = 24.dp),
                            image = R.drawable.ic_donate
                        )
                    }
                }
            }
        }
    }
}