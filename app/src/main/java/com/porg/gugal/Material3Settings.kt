/*
 *     Material3Settings.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

class Material3Settings {
    companion object {

        val padding = 20.dp
        val dividerPadding = 5.dp

        @Composable
        fun RegularSetting(title: String, body: String, onClick: (() -> Unit)) {
            Column(
                modifier = Modifier.clickable { onClick() }.fillMaxWidth(),
            ) {
                Text(
                    text = title,
                    modifier = Modifier.padding(start = padding, end = padding, top = padding, bottom = dividerPadding),
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = body,
                    modifier = Modifier.padding(start = padding, end = padding, bottom = padding),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }

        @Preview
        @Composable
        private fun RegularSettingPreview() {
            Surface() {
                RegularSetting("Title", "Body text", {})
            }
        }
    }
}