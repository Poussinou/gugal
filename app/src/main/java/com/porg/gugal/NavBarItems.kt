/*
 *     NavBarItems.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

object NavBarItems {
    val BarItems = listOf(
        BarItem(
            title = "Search",
            image = R.drawable.ic_search,
            route = "search"
        ),
        BarItem(
            title = "About",
            image = R.drawable.ic_info,
            route = "settings"
        )
    )
}