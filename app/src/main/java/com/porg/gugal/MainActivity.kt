/*
 *     MainActivity.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.ui.theme.GugalTheme

@ExperimentalAnimationApi
@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {

    // TODO SPFW: make SERP provider selectable
    val serpProvider: SerpProvider = GoogleCseSerp()

    private var apikey = ""
    private var cx = ""
    private lateinit var context: Context
    var showLoadOldPrefsAlert = false

    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ca: List<String>? = loadCxApi()
        if (ca?.get(0) ?: "none" != "none") cx = ca?.get(0) ?: ""
        if (ca?.get(1) ?: "none" != "none") apikey = ca?.get(1) ?: ""

        context = this

        setContent {
            GugalTheme {
                if (showLoadOldPrefsAlert) {
                    Column(modifier = Modifier.fillMaxWidth().fillMaxHeight(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally) {
                        Text("Updating save data...", Modifier.padding(bottom = 16.dp))
                        LinearProgressIndicator()
                    }
                    resaveGoogleNonSerpPrefs(cx, apikey)
                } else {
                    // A surface container using the 'background' color from the theme
                    Surface(color = MaterialTheme.colorScheme.background) {
                        val navController = rememberNavController()
                        Scaffold(
                            content = { NavigationHost(navController = navController) },
                            bottomBar = { BottomNavigationBar(navController = navController)},
                            backgroundColor = Color.Transparent,
                        )
                    }
                }
            }
        }
    }

    private fun resaveGoogleNonSerpPrefs(cx: String, apikey: String) {
        // Although you can define your own key generation parameter specification, it's
        // recommended that you use the value specified here.
        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val sharedPrefsFile = "gugalprefs"
        val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            sharedPrefsFile,
            mainKeyAlias,
            applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        with (sharedPreferences.edit()) {
            // Edit the user's shared preferences...
            this.putString("serp_${serpProvider.id}_cx", cx)
            this.putString("serp_${serpProvider.id}_ak", apikey)
            // ...and remove the old preferences
            this.remove("serp_google_data_cx")
            this.remove("serp_google_data_ak")
            apply()
        }
        showLoadOldPrefsAlert = false
    }


    private fun loadCxApi(): List<String>? {
        // Although you can define your own key generation parameter specification, it's
        // recommended that you use the value specified here.
        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val sharedPrefsFile = "gugalprefs"
        val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            sharedPrefsFile,
            mainKeyAlias,
            applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
        val arr: ArrayList<String> = ArrayList()
        if (sharedPreferences.getString("serp_google_data_cx", "none") != "none") {
            sharedPreferences.getString("serp_google_data_cx", "none")?.let { arr.add(it) }
            sharedPreferences.getString("serp_google_data_ak", "none")?.let { arr.add(it) }
            showLoadOldPrefsAlert = true;
            return (arr.toArray() as? Array<*>)?.filterIsInstance<String>()
        }
        sharedPreferences.getString("serp_${serpProvider.id}_cx", "none")?.let { arr.add(it) }
        sharedPreferences.getString("serp_${serpProvider.id}_ak", "none")?.let { arr.add(it) }
        return (arr.toArray() as? Array<*>)?.filterIsInstance<String>()
    }

    @Composable
    fun BottomNavigationBar(navController: NavHostController) {

        NavigationBar {
            val backStackEntry by navController.currentBackStackEntryAsState()
            val currentRoute = backStackEntry?.destination?.route

            NavBarItems.BarItems.forEach { navItem ->

                NavigationBarItem(
                    selected = currentRoute == navItem.route,
                    onClick = {
                        navController.navigate(navItem.route) {
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },

                    icon = {
                        Icon(imageVector = ImageVector.vectorResource(navItem.image),
                            contentDescription = navItem.title)
                    },
                    label = {
                        Text(text = navItem.title)
                    },
                )
            }
        }
    }

    @Composable
    fun NavigationHost(navController: NavHostController) {
        NavHost(
            navController = navController,
            startDestination = Routes.Search.route,
        ) {
            composable(Routes.Search.route) {
                ResultPage(context, apikey, cx)
            }

            composable(Routes.Settings.route) {
                SettingsPage(context)
            }
        }
    }
}